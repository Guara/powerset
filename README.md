## Exercise binary to get the powerset of a comma separated set

```bash
$ powerset 123,456,789

123
456
789
123,456
123,789
456,789
123,456,789
```
