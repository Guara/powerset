#![feature(test)]
#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![deny(clippy::nursery)]
#![deny(clippy::cargo)]

extern crate test;

use std::error::Error;
use std::fmt::Display;
use std::{env, io};

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    args.get(1).map_or_else(
        || {
            let stdin_set = set_from_stdin()?;
            print(&powerset(&stdin_set.split(',').collect::<Vec<&str>>()));
            Ok(())
        },
        |input| {
            print(&powerset(&input.split(',').collect::<Vec<&str>>()));
            Ok(())
        },
    )
}

/// The time complexity of this algorithm is n*2^n, for (n) each input set,
/// create a new set adding the element to the accumulated sets,
/// then add the new set to the accumulated sets.
/// On each iteration the accumulated sets grow, taking the complexity to an exponential form.
/// The spatial complexity is n2^n, given by the length of the output (2^n) and the local subset variable, allocated n times.
fn powerset<T>(set: &[T]) -> Vec<Vec<T>>
where
    T: Clone + Default,
{
    set.iter().fold(vec![vec![]], |mut subsets, set_elem| {
        let subset = subsets.clone().into_iter().map(|mut subset| {
            subset.push(set_elem.clone());
            subset
        });
        subsets.extend(subset);
        subsets
    })
}

fn print<T>(powerset: &[Vec<T>])
where
    T: Display,
{
    let last_line = powerset.len();
    for (line, set) in powerset.iter().enumerate() {
        for (i, elem) in set.iter().enumerate() {
            if i == 0 {
                print!("{}", elem)
            } else {
                print!(",{}", elem)
            }
        }
        if line < last_line {
            println!();
        }
    }
}

fn set_from_stdin() -> io::Result<String> {
    println!("waiting for input....");

    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut i = 0;
    loop {
        //minimal support for multiline input
        if i != 0 && !buffer.ends_with(',') {
            buffer.push(',');
        }
        if stdin.read_line(&mut buffer)? == 0 {
            break;
        };
        i += 1;
    }

    if buffer.ends_with(',') {
        buffer.truncate(buffer.len() - 1);
    }

    buffer.retain(|ch| !ch.is_whitespace());
    Ok(buffer)
}

#[cfg(test)]
mod tests {
    use std::error::Error;
    use test::Bencher;

    use super::powerset;

    #[test]
    fn test_powerset() -> Result<(), Box<dyn Error>> {
        let powerset = powerset(&["123", "456", "789"]);
        let correct_powerset = vec![
            vec![],
            vec!["123"],
            vec!["456"],
            vec!["123", "456"],
            vec!["789"],
            vec!["123", "789"],
            vec!["456", "789"],
            vec!["123", "456", "789"],
        ];
        assert_eq!(powerset, correct_powerset);
        Ok(())
    }

    #[test]
    fn test_empty_set() -> Result<(), Box<dyn Error>> {
        let powerset = powerset::<&str>(&[]);
        let correct_powerset: Vec<Vec<&str>> = vec![vec![]];
        assert_eq!(powerset, correct_powerset);
        Ok(())
    }

    #[bench]
    fn bench_powerset(b: &mut Bencher) {
        b.iter(|| powerset(&["1", "2", "3", "4", "5", "6", "7", "8", "9"]));
    }
}
